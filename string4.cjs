const str={"first_name": "JoHN", "last_name": "SMith"};
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"} ;

function getfullname(str){
    let fullname='';
    for(let name in str){
        fullname +=  str[name].charAt(0).toUpperCase() + str[name].slice(1).toLowerCase() + " ";
    }
    return fullname.trim();
}
console.log(getfullname(str));
module.exports = getfullname