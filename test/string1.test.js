const getNewStr = require('../string1.cjs');


test("provides new string " ,() => {
    expect(getNewStr(["$100.45", "$1,002.22", "-$123" ])).toStrictEqual([ 100.45, 1002.22, -123 ])
})