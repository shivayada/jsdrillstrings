const date= "10/1/2021";

function findmonth(date){
    let months= ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let start=0;
    let end=0;
    for(let index=0;index < date.length;index++){
        if(isNaN(date[index])){
            if(start!=0){
                end= index;
            }
            else{
                start=index+1
            }
        }
    }
    let getmonth= date.substring(start,end);
    return months[parseInt(getmonth)-1];
}

console.log(findmonth(date));

module.exports = findmonth