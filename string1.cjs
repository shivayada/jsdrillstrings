const str = ["$100.45", "$1,002.22", "-$123" ];

function getNewStr(str){
    let newStr =[];
    for(let index=0;index < str.length ; index++){
       let newsub = str[index].replaceAll('$', '');
       newsub = newsub.replaceAll(',','');
       if(!isNaN(newsub)){

        newStr.push(parseFloat(newsub));
       }else{
        newStr.push(0);
       }
    }
    return newStr;
}

console.log(getNewStr(str));

module.exports = getNewStr
